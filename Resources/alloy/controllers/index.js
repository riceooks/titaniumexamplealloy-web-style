function Controller() {
    function doClick(e) {
        alert($.label.text);
    }
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    $model = arguments[0] ? arguments[0].$model : null;
    var $ = this, exports = {};
    $.__views.index = A$(Ti.UI.createWindow({
        backgroundColor: "white",
        id: "index"
    }), "Window", null);
    $.addTopLevelView($.__views.index);
    $.__views.header = A$(Ti.UI.createView({
        zIndex: 50,
        backgroundColor: "red",
        height: "50sp",
        top: 0,
        id: "header"
    }), "View", $.__views.index);
    $.__views.index.add($.__views.header);
    $.__views.__alloyId1 = A$(Ti.UI.createLabel({
        width: Ti.UI.SIZE,
        height: Ti.UI.SIZE,
        color: "#000",
        text: "Header",
        id: "__alloyId1"
    }), "Label", $.__views.header);
    $.__views.header.add($.__views.__alloyId1);
    $.__views.body = A$(Ti.UI.createView({
        zIndex: 50,
        backgroundColor: "yellow",
        top: "50sp",
        bottom: "50sp",
        id: "body"
    }), "View", $.__views.index);
    $.__views.index.add($.__views.body);
    $.__views.__alloyId2 = A$(Ti.UI.createLabel({
        width: Ti.UI.SIZE,
        height: Ti.UI.SIZE,
        color: "#000",
        text: "Body",
        id: "__alloyId2"
    }), "Label", $.__views.body);
    $.__views.body.add($.__views.__alloyId2);
    $.__views.footer = A$(Ti.UI.createView({
        zIndex: 50,
        backgroundColor: "green",
        height: "50sp",
        bottom: 0,
        id: "footer"
    }), "View", $.__views.index);
    $.__views.index.add($.__views.footer);
    $.__views.__alloyId3 = A$(Ti.UI.createLabel({
        width: Ti.UI.SIZE,
        height: Ti.UI.SIZE,
        color: "#000",
        text: "Footer",
        id: "__alloyId3"
    }), "Label", $.__views.footer);
    $.__views.footer.add($.__views.__alloyId3);
    _.extend($, $.__views);
    $.index.open();
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._, A$ = Alloy.A, $model;

module.exports = Controller;